<?php

declare(strict_types=1);

namespace Tests\FileReader;

use App\Exceptions\FileNotFoundException;
use App\FileReader\File;
use Illuminate\Support\LazyCollection;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class FileTest extends TestCase
{
    private static string $dataDir = __DIR__.'/../../data';

    public function testLines(): void
    {
        $path = self::$dataDir.'/temp.txt';

        $contents = LazyCollection::times(3)
            ->map(function ($number) {
                return "line-{$number}";
            })
            ->join("\n");

        file_put_contents($path, $contents);

        $files = new File();
        FileTest::assertInstanceOf(LazyCollection::class, $files->lines($path));

        FileTest::assertSame(
            ['line-1', 'line-2', 'line-3'],
            $files->lines($path)->all()
        );

        unlink($path);
    }

    public function testLinesThrowsExceptionWhenFileDoesNotExist(): void
    {
        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('File does not exist at path /nonexistence.txt.');

        $filesystem = new File();
        $filesystem->lines('/nonexistence.txt');
    }

    public function testIsFileChecksFilesProperly(): void
    {
        $filesystem = new File();

        FileTest::assertTrue($filesystem->isFile(self::$dataDir.'/input.txt'));
        FileTest::assertFalse($filesystem->isFile(self::$dataDir.'./nonexistence.txt'));
    }
}
