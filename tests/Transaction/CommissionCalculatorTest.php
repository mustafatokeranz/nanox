<?php

declare(strict_types=1);

namespace Tests\Transaction;

use App\BinProviders\BinDTO;
use App\BinProviders\BinHttpClient;
use App\Exceptions\CurrencyRateNotFoundException;
use App\ExchangeRateProviders\ExchangeRateDTO;
use App\ExchangeRateProviders\ExchangeRateHttpClient;
use App\Transaction\CommissionCalculator;
use App\Transaction\TransactionDTO;
use Mockery;
use Mockery\Adapter\Phpunit\MockeryTestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class CommissionCalculatorTest extends MockeryTestCase
{
    private ExchangeRateHttpClient $exchangeRateHttpClientMock;

    protected function setUp(): void
    {
        parent::setUp();

        // Mock the ExchangeRateHttpClient
        $this->exchangeRateHttpClientMock = Mockery::mock(ExchangeRateHttpClient::class);
        $exchangeRateDTO = new ExchangeRateDTO([
            'USD' => 1.099002,
            'JPY' => 145.276669,
            'GBP' => 0.885768,
            'EUR' => 1.0,
        ]);

        $this->exchangeRateHttpClientMock
            ->shouldReceive('getRates')
            ->andReturn($exchangeRateDTO)
        ;
    }

    public function testCalculateForEu(): void
    {
        // Mock the BinHttpClient
        $binHttpClientMock = Mockery::mock(BinHttpClient::class);
        $binDTO = new BinDTO('FR', 'EUR');

        $binHttpClientMock
            ->shouldReceive('lookUp')
            ->with('123456')
            ->andReturn($binDTO);

        $calculator = new CommissionCalculator($binHttpClientMock, $this->exchangeRateHttpClientMock);

        $transactionDTO = new TransactionDTO('123456', 100.0, 'EUR');

        $commission = $calculator->calculate($transactionDTO);

        CommissionCalculatorTest::assertSame(1.0, $commission);
    }

    public function testCalculateForNonEu(): void
    {
        // Mock the BinHttpClient
        $binHttpClientMock = Mockery::mock(BinHttpClient::class);
        $binDTO = new BinDTO('JP', 'JPY');

        $binHttpClientMock
            ->shouldReceive('lookUp')
            ->with('123456')
            ->andReturn($binDTO);

        // Instantiate the CommissionCalculator
        $calculator = new CommissionCalculator($binHttpClientMock, $this->exchangeRateHttpClientMock);

        // Create a TransactionDTO
        $transactionDTO = new TransactionDTO('123456', 10000.00, 'JPY');

        $commission = $calculator->calculate($transactionDTO);

        CommissionCalculatorTest::assertSame(1.38, $commission);
    }

    public function testCalculateWithInvalidCurrency(): void
    {
        // Mock the BinHttpClient
        $binHttpClientMock = Mockery::mock(BinHttpClient::class);
        $binDTO = new BinDTO('AT', 'EUR');
        $binHttpClientMock
            ->shouldReceive('lookUp')
            ->with('123456')
            ->andReturn($binDTO);

        $calculator = new CommissionCalculator($binHttpClientMock, $this->exchangeRateHttpClientMock);

        // Create a TransactionDTO with an invalid currency
        $transactionDTO = new TransactionDTO('123456', 100.0, 'INVALID');

        $this->expectException(CurrencyRateNotFoundException::class);
        $calculator->calculate($transactionDTO);
    }
}
