<?php

declare(strict_types=1);

namespace Tests\BinProviders;

use App\BinProviders\BinDTO;
use App\BinProviders\BinListHttpHttpClient;
use App\Exceptions\InvalidJsonException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 *
 * @coversNothing
 */
final class BinListHttpHttpClientTest extends TestCase
{
    use MockeryPHPUnitIntegration;

    public function testLookUpSuccess(): void
    {
        $binNumber = '123456';
        $binDTO = new BinDTO('DK', 'DKK');

        $httpClientMock = \Mockery::mock(Client::class);
        $httpClientMock->shouldReceive('request')
            ->once()
            ->with('GET', $binNumber)
            ->andReturn(
                new Response(200, [], json_encode([
                    'country' => ['alpha2' => 'DK', 'currency' => 'DKK'],
                ])),
            );

        $binListHttpHttpClient = new BinListHttpHttpClient($httpClientMock);

        $result = $binListHttpHttpClient->lookUp($binNumber);

        $this->assertEquals($binDTO, $result);
    }

    public function testLookUpFailure(): void
    {
        $binNumber = '123456';

        $httpClientMock = \Mockery::mock(Client::class);
        $httpClientMock->shouldReceive('request')
            ->once()
            ->with('GET', $binNumber)
            ->andReturn(
                new Response(400, []),
            );

        $binListHttpHttpClient = new BinListHttpHttpClient($httpClientMock);

        $this->expectException(\RuntimeException::class);
        $binListHttpHttpClient->lookUp($binNumber);
    }

    public function testLookUpFailureWhenReturnsInvalidResponse(): void
    {
        $binNumber = '123456';

        $httpClientMock = \Mockery::mock(Client::class);
        $httpClientMock->shouldReceive('request')
            ->once()
            ->with('GET', $binNumber)
            ->andReturn(
                new Response(200, [], 'Something went wrong while processing your request. Please try again later.'),
            );

        $binListHttpHttpClient = new BinListHttpHttpClient($httpClientMock);

        $this->expectException(InvalidJsonException::class);
        $binListHttpHttpClient->lookUp($binNumber);
    }
}
