<?php

declare(strict_types=1);

use App\BinProviders\BinListHttpHttpClient;
use App\Exceptions\CurrencyRateNotFoundException;
use App\Exceptions\FileNotFoundException;
use App\ExchangeRateProviders\ExchangeRatesApiHttpClient;
use App\FileReader\File;
use App\Transaction\TransactionProcessor;
use GuzzleHttp\Client;

require_once __DIR__.'/vendor/autoload.php';

$fileName = $argv[1] ?? null;

if (!$fileName) {
    throw new InvalidArgumentException('Please provide a file name as the first argument.');
}

try {
    $binListHttpClient = new BinListHttpHttpClient(new Client(config: [
        'base_uri' => 'https://lookup.binlist.net/',
    ]));

    $exchangeRateHttpClient = new ExchangeRatesApiHttpClient(new Client(config: [
        'base_uri' => 'https://api.exchangerate.host/',
        'headers' => [
            'Accept' => 'application/json',
            'Content-Type' => 'text/plain',
            'apikey' => 'zFaGwQK3Gdn4iwflk4l5LiTy1IkCzkYN',
        ],
    ]));

    $transactionProcessor = new TransactionProcessor($binListHttpClient, $exchangeRateHttpClient, new File());
    $listOfCommissions = $transactionProcessor->process(__DIR__.'/'.$fileName);

    foreach ($listOfCommissions as $commission) {
        echo $commission.PHP_EOL;
    }
} catch (FileNotFoundException $e) {
    throw new InvalidArgumentException('File not found.');
} catch (CurrencyRateNotFoundException $e) {
    throw new InvalidArgumentException('Currency rate not found.');
}
