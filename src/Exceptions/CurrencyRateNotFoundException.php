<?php

declare(strict_types=1);

namespace App\Exceptions;

class CurrencyRateNotFoundException extends \Exception
{
    public function __construct(string $message = null)
    {
        $message ??= 'Currency rate not found';

        parent::__construct($message);
    }
}
