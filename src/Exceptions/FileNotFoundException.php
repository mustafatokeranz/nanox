<?php

declare(strict_types=1);

namespace App\Exceptions;

class FileNotFoundException extends \Exception
{
    protected string $path;

    public function __construct($path, $code = 0, \Exception $previous = null)
    {
        $this->path = $path;

        parent::__construct('File not found at path: '.$this->getPath(), $code, $previous);
    }

    /**
     * Get the path which was not found.
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
