<?php

declare(strict_types=1);

namespace App;

use App\Exceptions\InvalidJsonException;

class Json
{
    public static function parse(string $json, int $flags = 0): array
    {
        $decodedJSON = json_decode($json, true, 512, $flags);

        $hasJsonError = JSON_ERROR_NONE !== json_last_error();

        if ($hasJsonError) {
            throw new InvalidJsonException('Invalid JSON provided.');
        }

        return $decodedJSON;
    }
}
