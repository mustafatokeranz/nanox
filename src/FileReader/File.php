<?php

declare(strict_types=1);

namespace App\FileReader;

use App\Exceptions\FileNotFoundException;
use Illuminate\Support\LazyCollection;

class File
{
    /**
     * @throws FileNotFoundException
     */
    public static function lines(string $path): LazyCollection
    {
        if (!self::isFile($path)) {
            throw new FileNotFoundException("File does not exist at path {$path}.");
        }

        return LazyCollection::make(function () use ($path): \Generator {
            $file = new \SplFileObject($path);
            $file->setFlags(\SplFileObject::DROP_NEW_LINE);

            while (!$file->eof()) {
                yield $file->fgets();
            }
        });
    }

    public static function isFile($file): bool
    {
        return is_file($file);
    }
}
