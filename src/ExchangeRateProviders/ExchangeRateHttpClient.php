<?php

declare(strict_types=1);

namespace App\ExchangeRateProviders;

interface ExchangeRateHttpClient
{
    public function getRates(): ExchangeRateDTO;
}
