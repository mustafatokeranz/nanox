<?php

declare(strict_types=1);

namespace App\ExchangeRateProviders;

use App\Json;

readonly class ExchangeRateDTO
{
    public function __construct(public array $rates)
    {
        // ...
    }

    public static function fromJson(string $json, int $flags = 0): self
    {
        $decodedJson = Json::parse($json, $flags);

        if (!isset($decodedJson['rates'])) {
            throw new \InvalidArgumentException('Invalid JSON provided.');
        }

        return new self(
            $decodedJson['rates'],
        );
    }
}
