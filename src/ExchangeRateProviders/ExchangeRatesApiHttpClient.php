<?php

declare(strict_types=1);

namespace App\ExchangeRateProviders;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

readonly class ExchangeRatesApiHttpClient implements ExchangeRateHttpClient
{
    public function __construct(private Client $httpClient)
    {
        // ...
    }

    /**
     * @throws \Exception
     */
    public function getRates(): ExchangeRateDTO
    {
        try {
            $exchangeRateResponse = $this->httpClient->request('GET', 'latest');
        } catch (GuzzleException $e) {
            throw new \RuntimeException('Bin list lookup has failed.');
        }

        if (200 !== $exchangeRateResponse->getStatusCode()) {
            throw new \RuntimeException('Exchange rate lookup has failed.');
        }

        return ExchangeRateDTO::fromJson($exchangeRateResponse->getBody()->getContents());
    }
}
