<?php

declare(strict_types=1);

namespace App\BinProviders;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

readonly class BinListHttpHttpClient implements BinHttpClient
{
    public function __construct(private Client $httpClient)
    {
        // ...
    }

    public function lookUp(string $binNumber): BinDTO
    {
        try {
            $binListResponse = $this->httpClient->request('GET', $binNumber);
        } catch (GuzzleException $e) {
            throw new \RuntimeException('Bin list lookup has failed.');
        }

        $isSuccessful = 200 !== $binListResponse->getStatusCode();

        if ($isSuccessful) {
            throw new \RuntimeException('Bin list lookup has failed.');
        }

        return BinDTO::fromJson($binListResponse->getBody()->getContents());
    }
}
