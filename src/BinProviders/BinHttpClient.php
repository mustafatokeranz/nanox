<?php

declare(strict_types=1);

namespace App\BinProviders;

interface BinHttpClient
{
    public function lookUp(string $binNumber): BinDTO;
}
