<?php

declare(strict_types=1);

namespace App\BinProviders;

use App\Json;

readonly class BinDTO
{
    public function __construct(public string $country, public string $currency)
    {
        // ...
    }

    public static function fromJson(string $json, int $flags = 0): self
    {
        $decodedJson = Json::parse($json, $flags);

        if (!isset($decodedJson['country']['alpha2'], $decodedJson['country']['currency'])) {
            throw new \InvalidArgumentException('Invalid JSON provided.');
        }

        return new self(
            $decodedJson['country']['alpha2'],
            $decodedJson['country']['currency'],
        );
    }
}
