<?php

declare(strict_types=1);

namespace App\Transaction;

use App\BinProviders\BinListHttpHttpClient;
use App\Exceptions\CurrencyRateNotFoundException;
use App\Exceptions\FileNotFoundException;
use App\ExchangeRateProviders\ExchangeRatesApiHttpClient;
use App\FileReader\File;

readonly class TransactionProcessor
{
    public function __construct(
        private BinListHttpHttpClient $binListHttpClient,
        private ExchangeRatesApiHttpClient $exchangeRateHttpClient,
        private File $fileReader,
    ) {
        // ...
    }

    /**
     * @throws CurrencyRateNotFoundException
     * @throws FileNotFoundException
     */
    public function process(string $fileName): array
    {
        $transactions = $this->fileReader->lines($fileName);

        $listOfCommissions = [];

        foreach ($transactions as $transaction) {
            $transactionDTO = TransactionDTO::fromJson($transaction);

            $commissionCalculator = new CommissionCalculator(
                $this->binListHttpClient,
                $this->exchangeRateHttpClient
            );

            $listOfCommissions[] = $commissionCalculator->calculate($transactionDTO);
        }

        return $listOfCommissions;
    }
}
