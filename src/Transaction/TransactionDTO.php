<?php

declare(strict_types=1);

namespace App\Transaction;

readonly class TransactionDTO
{
    public function __construct(
        public string $bin,
        public float $amount,
        public string $currency,
    ) {
        // ...
    }

    public static function fromJson(string $json, int $flags = 0): self
    {
        $decodedJson = json_decode($json, true, 512, $flags);

        if (!isset($decodedJson['bin'], $decodedJson['amount'], $decodedJson['currency'])) {
            throw new \InvalidArgumentException('Invalid JSON provided.');
        }

        return new self(
            $decodedJson['bin'],
            (float) $decodedJson['amount'],
            $decodedJson['currency']
        );
    }
}
