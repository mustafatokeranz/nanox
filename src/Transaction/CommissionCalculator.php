<?php

declare(strict_types=1);

namespace App\Transaction;

use App\BinProviders\BinHttpClient;
use App\Exceptions\CurrencyRateNotFoundException;
use App\ExchangeRateProviders\ExchangeRateHttpClient;
use App\Transaction;

class CommissionCalculator
{
    private const COMMISSION_RATE_FOR_EU = 0.01;

    private const COMMISSION_RATE_FOR_OTHERS = 0.02;

    private const DEFAULT_CURRENCY = 'EUR';

    private const DEFAULT_PRECISION = 2;

    private static array $euCountries = [
        'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK',
        'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU',
        'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL',
        'PO', 'PT', 'RO', 'SE', 'SI', 'SK',
    ];

    public function __construct(
        private readonly BinHttpClient $binHttpClient,
        private readonly ExchangeRateHttpClient $exchangeRateHttpClient,
    ) {
        // ...
    }

    /**
     * @throws CurrencyRateNotFoundException
     */
    public function calculate(Transaction\TransactionDTO $transactionDTO): float
    {
        $exchangeRateDTO = $this->exchangeRateHttpClient->getRates();

        $rateOfCurrency = $exchangeRateDTO->rates[$transactionDTO->currency] ?? null;

        if (!$rateOfCurrency) {
            throw new CurrencyRateNotFoundException();
        }

        $amount = $transactionDTO->amount;

        if (self::DEFAULT_CURRENCY !== $transactionDTO->currency || $rateOfCurrency > 0) {
            $amount = $transactionDTO->amount / $rateOfCurrency;
        }

        $binDTO = $this->binHttpClient->lookUp($transactionDTO->bin);

        $isEu = $this->isEu($binDTO->country);

        $commissionRate = $isEu ? self::COMMISSION_RATE_FOR_EU
            : self::COMMISSION_RATE_FOR_OTHERS;

        return round($amount * $commissionRate, self::DEFAULT_PRECISION);
    }

    private function isEu(string $shortCountryCode): bool
    {
        return \in_array($shortCountryCode, self::$euCountries, true);
    }
}
