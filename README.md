# NanoX

## Getting started

Run the following commands to get started:

```bash
git clone https://gitlab.com/mustafatokeranz/nanox.git
cd nanox
composer install

php ugly.php data/input.txt // for running ugly code

php pretty.php data/input.txt // for running pretty code
```